<?php

namespace endlessdreams\taxtools\grin\Helpers;


use endlessdreams\taxtools\gbif\Helpers\GbifJsonHelper;


/**
 * Grin Helper
 * 
 * A toolkit with to GRIN json API v1.
 * 
 * All calls allows different parameters. Here is the list of the parameters.
 * 
 * Parameter        Description
 * class            Optional class classification accepting a canonical name.
 * datasetKey       Filters by the checklist dataset key (a uuid)
 * facet            A list of facet names used to retrieve the 100 most 
 *                  frequent values for a field. Allowed facets are: datasetKey, 
 *                  higherTaxonKey, rank, status, nomenclaturalStatus, 
 *                  isExtinct, habitat, threat and nameType.
 * facetMincount    Used in combination with the facet parameter. Set 
 *                  facetMincount={#} to exclude facets with a count less than 
 *                  {#}, e.g. /search?facet=type&limit=0&facetMincount=10000 
 *                  only shows the type value 'OCCURRENCE' because 'CHECKLIST' 
 *                  and 'METADATA' have counts less than 10000.
 * facetMultiselect	Used in combination with the facet parameter. Set 
 *                  facetMultiselect=true to still return counts for values 
 *                  that are not currently filtered, 
 *                  e.g. /search?facet=type&limit=0&type=CHECKLIST&facetMultiselect=true 
 *                  still shows type values 'OCCURRENCE' and 'METADATA' even 
 *                  though type is being filtered by type=CHECKLIST
 * family           Optional family classification accepting a canonical name.
 * genus            Optional genus classification accepting a canonical name.
 * habitat          Filters by the habitat. Currently only 3 major biomes are 
 *                  accepted in our Habitat enum
 * highertaxonKey	Filters by any of the higher Linnean rank keys. Note this 
 *                  is within the respective checklist and not searching nub 
 *                  keys across all checklists.
 * hl               Set hl=true to highlight terms matching the query when 
 *                  in fulltext search fields. The highlight will be an 
 *                  emphasis tag of class 'gbifH1' 
 *                  e.g. /search?q=plant&hl=true. Fulltext search fields 
 *                  include: title, keyword, country, publishing country, 
 *                  publishing organization title, hosting organization title, 
 *                  and description. One additional full text field is 
 *                  searched which includes information from metadata 
 *                  documents, but the text of this field is not returned in 
 *                  the response.
 * isExtinct        Filters by extinction status (a boolean, e.g. 
 *                  isExtinct=true)
 * issue            A specific indexing issue as defined in our 
 *                  NameUsageIssue enum
 * kingdom          Optional kingdom classification accepting a canonical name.
 * language         Language for vernacular names. Overrides HTTP 
 *                  Accept-Language header
 * name             A scientific name which can be either a case insensitive 
 *                  filter for a canonical namestring, e.g. 'Puma concolor', 
 *                  or an input to the name parser
 * nameType         Filters by the name type as given in our NameType enum
 * nomenclaturalStatus	Not yet implemented, but will eventually allow for 
 *                  filtering by a nomenclatural status enum
 * order            Optional order classification accepting a canonical name.
 * phylum           Optional phylum classification accepting a canonical name.
 * q                Simple full text search parameter. The value for this 
 *                  parameter can be a simple word or a phrase. Wildcards are 
 *                  not supported
 * rank             Filters by taxonomic rank as given in our Rank enum
 * sourceId         Filters by the source identifier
 * status           Filters by the taxonomic status as given in our 
 *                  TaxonomicStatus enum
 * strict           If true it (fuzzy) matches only the given name, but never 
 *                  a taxon in the upper classification
 * threat           Not yet implemented, but will eventually allow for 
 *                  filtering by a threat status enum
 * verbose          If true it shows alternative matches which were considered 
 *                  but then rejected
 *                  
 * When return type is a paged array.
 * limit            Controls the number of results in the page. Using too high 
 *                  a value will be overwritten with the default maximum 
 *                  threshold, depending on the service. Sensible defaults are 
 *                  used so this may be omitted.
 * offset           Determines the offset for the search results. A limit of 
 *                  20 and offset of 20, will get the second page of 20 results.
 * 
 * @author Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @since v0.0.1
 * 
 * 
 *
 */
class GrinJsonHelper extends GbifJsonHelper {

    /**
     * Grin datasetKey
     * 
     * @var array
     */
    protected static $params = ['datasetKey' => '66dd0960-2d7d-46ee-a491-87b9adcfe7b1']; 
}