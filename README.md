# README #



### What is this repository for? ###

This repository is for utils of GRIN web services.
The U.S. National Plant Germplasm System (NPGS) has a taxonomy database,
which is searchable through webformulares.
(https://npgsweb.ars-grin.gov/gringlobal/taxon/abouttaxonomy.aspx)
There seems not be a web service API, but all their datasets are uploaded
to GBIF -- Global Biodiversity Information Facility.
(http://www.gbif.org/)
By using the web services of GBIF, and filtering over Grin-globals datasets
in a way a web services API has bin implemented.

### Installation ###

The preferred way to install this extension is through composer.

Either run

php composer.phar require --dev --prefer-dist endlessdreams/taxtools-grin
or add

"endlessdreams/taxtools-grin": "*"
to the require-dev section of your composer.json file.

### Usage ###

var_dump(GrinJsonHelper::listAllNameUsagesAcrossAllChecklistsSearch(['q'=>'Acanthaceae']));

### Who do I talk to? ###

Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>